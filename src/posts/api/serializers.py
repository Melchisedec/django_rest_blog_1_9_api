from rest_framework.serializers import ModelSerializers

from posts.models import Post

class PostSerializer( ModelSerializers ):
	class Meta:
		model = Post
		fields = [
			'title',
			'slug',
			'content'
		]